package py.edu.ucom.is2.proyectocamel.transferencias;


import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.jsonpath.JsonPath;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class TransferenciaProducer extends RouteBuilder{
	
	private JacksonDataFormat jsonDataFormat;

	@Autowired
	TransferenciaService service;
	
//	@Autowired
//	TransferenciaResponse respuesta;

	@Override
	public void configure() throws Exception {
			
		jsonDataFormat = new JacksonDataFormat(TransferenciaRequest.class);
		
		restConfiguration().component("servlet").bindingMode(RestBindingMode.auto);
		
		rest().path("/api")
			.consumes("application/json")
			.produces("application/json")
			
		.post("/transferencia")
			.type(TransferenciaRequest.class)
			.outType(TransferenciaResponse.class)
			.to("direct:procesarTransferencia");
			
				
		from("direct:procesarTransferencia")
			.bean(service,"genDateID")
			
			.filter().method(FiltroTransferencia.class,"validarMonto")
							
				.to("direct:procesarSwitch").stop()
				
				.end()

			.bean(service,"MontoPermitido");
		
		
		//Encolamos los Mensajes recibidos
		from("direct:procesarSwitch")
		.log("Iniciando Productor")
		.marshal(jsonDataFormat)
			.choice()
			.when().jsonpath("$.[?(@.banco_destino == 'ATLAS')]")
				.setExchangePattern(ExchangePattern.InOnly)
				.to("activemq:Martinez-ATLAS-IN")
				.setExchangePattern(ExchangePattern.InOut)
				.unmarshal(jsonDataFormat)
				.bean(service,"result")
				.endChoice()
			.when().jsonpath("$.[?(@.banco_destino == 'ITAU')]")
				.setExchangePattern(ExchangePattern.InOnly)
				.to("activemq:Martinez-ITAU-IN")
				.setExchangePattern(ExchangePattern.InOut)
				.unmarshal(jsonDataFormat)
				.bean(service,"result")
				.endChoice()
			.when().jsonpath("$.[?(@.banco_destino == 'FAMILIAR')]")
				.setExchangePattern(ExchangePattern.InOnly)
				.to("activemq:Martinez-FAMILIAR-IN")
				.setExchangePattern(ExchangePattern.InOut)
				.unmarshal(jsonDataFormat)
				.bean(service,"result")
				.endChoice()
			
			.otherwise()
				
				.transform().constant("El valor enviado no es valido")
		    .end();
		
	}

}
