package py.edu.ucom.is2.proyectocamel.transferencias;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


//@Component
public class TransferenciaProcessorCola implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		TransferenciaRequest bancoRequest = exchange.getIn().getBody(TransferenciaRequest.class);
				
		if(bancoRequest != null) {
		
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			   
			   System.out.println(dtf.format(now).equals(bancoRequest.getFecha()));
			
			System.out.println("Banco Origen: " +bancoRequest.getBanco_origen() + " Banco Destino: " +bancoRequest.getBanco_destino() + 
					" Cuenta: " +bancoRequest.getCuenta() + " Monto: " +bancoRequest.getMonto() + " transferencia exitosa" );


			
		}
		else
			System.err.print("TransferenciaProcessorCola NULL");
	}
}
