package py.edu.ucom.is2.proyectocamel.transferencias;




import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TranferenciaConsumerATLAS extends RouteBuilder{

	private JacksonDataFormat jsonDataFormat;
	
	@Autowired
	TransferenciaService service;
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		
		jsonDataFormat = new JacksonDataFormat(TransferenciaRequest.class);

	
		from("activemq:Martinez-ATLAS-IN")
		.log("Consumidor ATLAS-IN")
		.unmarshal(jsonDataFormat)
		.process(new TransferenciaAgregarHeaderProcessor())
		.filter().method(FiltroTransferencia.class,"validarFecha")
			.to("direct:procesarSwitchAceptadoATLAS").stop()
			.end()

		.to("direct:procesarSwitchRechazadoATLAS").stop()
		.end();
			
		from("direct:procesarSwitchAceptadoATLAS")
		.log("Validacion Fechas Aceptados")
			.bean(service,"TransferenciaAceptada")
			.choice()
			.when(header("banco_origen").contains("ITAU"))
			.log("Banco origen ITAU")
			.setExchangePattern(ExchangePattern.InOnly)
			.marshal(jsonDataFormat)
			.to("activemq:Martinez-ITAU-OUT")
			.endChoice()
			.when(header("banco_origen").contains("ATLAS"))
				.log("Banco origen ATLAS")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ATLAS-OUT")
				.endChoice()
			.when(header("banco_origen").contains("FAMILIAR"))
				.log("Banco origen FAMILIAR")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-FAMILIAR-OUT")
				.endChoice()
			.otherwise()
				.log("Ninguna de las opciones")
			.end();
			
		from("direct:procesarSwitchRechazadoATLAS")
		.log("Validacion Fechas Rechazado")
			.bean(service,"TransferenciaRechazada")
			.choice()
			.when(header("banco_origen").contains("ATLAS"))
				.log("Banco origen ATLAS")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ATLAS-OUT")
				.endChoice()
			.when(header("banco_origen").contains("ITAU"))
				.log("Banco origen ITAU")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ITAU-OUT")
				.endChoice()
			.when(header("banco_origen").contains("FAMILIAR"))
				.log("Banco origen FAMILIAR")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-FAMILIAR-OUT")
				.endChoice()	
			.otherwise()
				.log("Ninguna de las opciones")
			.end();
				
	}

	

	
	

}
