package py.edu.ucom.is2.proyectocamel.transferencias;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Component;

@Component
public class TransferenciaService {
	public TransferenciaRequest genDateID(TransferenciaRequest genDateID) {
		
		int int_random = ThreadLocalRandom.current().nextInt(1 , 999999999) ;  

		genDateID.setNumtransferencia(int_random);
		
		if(genDateID.getFecha() == null) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		    LocalDateTime now = LocalDateTime.now();
		    //System.out.println(dtf.format(now));
		    	genDateID.setFecha(dtf.format(now));
		}
		return genDateID;
		
		}
	
	public TransferenciaRequest getDate(TransferenciaRequest getDate) {
		
				
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		   LocalDateTime now = LocalDateTime.now();
		   //System.out.println(dtf.format(now));
		   getDate.setFecha(dtf.format(now));

		return getDate;
		
		}	
	
	
	public String MontoPermitido(TransferenciaRequest request) {
		TransferenciaResponse respuesta = new TransferenciaResponse();
		respuesta.setMensaje(" El Monto de la Transferencia supera lo permitido " + 
				" ID de Transaccion: " +request.getNumtransferencia() + 
				" Monto : " +request.getMonto() );
		
		return respuesta.getMensaje();
	}
	
		
	public String result(TransferenciaRequest request) {
		TransferenciaResponse respuesta = new TransferenciaResponse();
		respuesta.setMensaje(" Tranferencia Encolada -  Banco Destino: " +request.getBanco_destino() + " -> ID de Transaccion: " +request.getNumtransferencia() );
		return respuesta.getMensaje();
	}
	
	public TransferenciaResponse TransferenciaAceptada(TransferenciaRequest request) {
		TransferenciaResponse respuesta = new TransferenciaResponse();
		respuesta.setMensaje(" Transferencia procesada exitosamente ");
		respuesta.setIdTransaccion(request.getNumtransferencia());
		return respuesta;
	}
	public TransferenciaResponse TransferenciaRechazada(TransferenciaRequest request) {
		TransferenciaResponse respuesta = new TransferenciaResponse();
		respuesta.setMensaje(" Tranferencia Rechazada - Mensaje caducado" );
		respuesta.setIdTransaccion(request.getNumtransferencia());
		return respuesta;
	}
	public TransferenciaResponse TransferenciaRespuesta(TransferenciaRequest request) {
		TransferenciaResponse respuesta = new TransferenciaResponse();
		respuesta.setMensaje(" Tranferencia Rechazada - Mensaje caducado" );
		respuesta.setIdTransaccion(request.getNumtransferencia());
		return respuesta;
	}

}
