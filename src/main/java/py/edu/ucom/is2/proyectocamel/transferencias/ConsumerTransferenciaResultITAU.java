package py.edu.ucom.is2.proyectocamel.transferencias;


import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
import org.apache.camel.component.jackson.JacksonDataFormat;


@Component
public class ConsumerTransferenciaResultITAU extends RouteBuilder{
	
	private JacksonDataFormat jsonDataFormat;
	
		
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
			
		jsonDataFormat = new JacksonDataFormat(TransferenciaResponse.class);
		
		from("activemq:Martinez-ITAU-OUT")
		.log("Consumidor ITAU-OUT")
		.unmarshal(jsonDataFormat)
		.process(new TransferenciaProcessorColaResult())
		.end();
		
	}


}

