package py.edu.ucom.is2.proyectocamel.routes.expiration;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import py.edu.ucom.is2.proyectocamel.helper.MessageGenerator;

//@Component
public class ExpirationProducer extends RouteBuilder{
	
	@Autowired
	MessageGenerator gen;

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		from("timer:active-mq-timer?period=500")
		.process(gen)
		.log("Mensaje enviado VM-${body}")
		.to("activemq:expirationTest?explicitQosEnabled=true&timeToLive=1000");
		
		
	}

}
