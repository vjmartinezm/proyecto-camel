package py.edu.ucom.is2.proyectocamel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class FileRouter extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// Copia archivos de la carpeta in a la carpeta output
		from("file:archivoss/input")
		.log("${body}")
		.to("file:archivos/output");
		
	}

}
