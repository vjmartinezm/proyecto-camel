package py.edu.ucom.is2.proyectocamel.routes.choice;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import py.edu.ucom.is2.proyectocamel.bancos.BancoService;


//@Component
public class ChoiceRestTest extends RouteBuilder{

	@Autowired
	BancoService service;
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		restConfiguration().component("servlet").bindingMode(RestBindingMode.auto);
		
		rest().path("/banco")
			.consumes("application/json")
			.produces("application/json")
			
		.post("/mensaje")
			.to("direct:procesarSwitch");
		
		//Route que va a procesar el post enviar
		from("direct:procesarSwitch")
			.choice()
			.when(header("origen").contains("IS1")).to("log:is1Logger").endChoice()
			.when(header("origen").contains("IS2")).to("log:is2sLogger").endChoice()
			.otherwise()
				.transform().constant("El valor enviado no es valido");
		
	}

}
