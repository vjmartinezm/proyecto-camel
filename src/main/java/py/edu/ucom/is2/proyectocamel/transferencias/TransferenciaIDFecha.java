package py.edu.ucom.is2.proyectocamel.transferencias;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Component;

//@Component
public class TransferenciaIDFecha {

	public TransferenciaRequest genId(TransferenciaRequest transfID) {

		int int_random = ThreadLocalRandom.current().nextInt(1 , 999999999) ;  

		transfID.setNumtransferencia(int_random);
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		   LocalDateTime now = LocalDateTime.now();
		   //System.out.println(dtf.format(now));
		   transfID.setFecha(dtf.format(now));

		return transfID;

	}

}

