package py.edu.ucom.is2.proyectocamel.bancos;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


//@Component
public class ProcessorCola implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		BancoRequest bancoRequest = exchange.getIn().getBody(BancoRequest.class);
		//System.out.println(bancoRequest);
				
		if(bancoRequest != null) {
			
			System.out.println("Usando PROCESS: Banco Origen: " +bancoRequest.getBanco_origen() + " Banco Destino: " +bancoRequest.getBanco_destino() + 
					" Cuenta: " +bancoRequest.getCuenta() + " Monto: " +bancoRequest.getMonto() + " transferencia exitosa" );


			
		}
		else
			System.err.print("bancoService NULL");
	}
}
