package py.edu.ucom.is2.proyectocamel.routes.mq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;


//@Component
public class ActiveMQTopicConsumer extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		from("activemq:topic:whasappMQ")
		.transform().simple("Consumidor ID 1 > ${body}")
		.to("log:is2log");
		
	}

}
